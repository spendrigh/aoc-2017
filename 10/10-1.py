inputFile = open("input.txt", "r+")

lengths = inputFile.read()

#lengths = "3,4,1,5"

lengths = map(lambda x: int(x),lengths.split(","))

listNumbers = range(0,256)

currentPosition = 0
skipSize = 0

def getListOfNumbersToSwap(lengthToSwap) :
    listOfNumbersSwapping = []
    if currentPosition + lengthToSwap >= len(listNumbers) :
        overFlow = currentPosition + lengthToSwap - len(listNumbers)
        listOfNumbersSwapping = listNumbers[ currentPosition : ] + listNumbers[ 0 : overFlow ]
    else :
        listOfNumbersSwapping = listNumbers[ currentPosition : (currentPosition+lengthToSwap) ]
    return listOfNumbersSwapping

def saveListOfNumbers(listOfNumbersSwapping) :
    global listNumbers
    if currentPosition + len(listOfNumbersSwapping) >= len(listNumbers) :
        overFlow = currentPosition + len(listOfNumbersSwapping) - len(listNumbers)
        firstList = listOfNumbersSwapping[ 0 : (len(listNumbers) - currentPosition) ]
        secondList = listOfNumbersSwapping[ (len(listNumbers) - currentPosition) : ]
        listNumbers = secondList + listNumbers[len(secondList) : -len(firstList) ] + firstList
    else :
        listNumbers = listNumbers[:currentPosition] + listOfNumbersSwapping + listNumbers[currentPosition+len(listOfNumbersSwapping):]

def goThroughNumbers(lengthToSwap) :
    global skipSize
    global currentPosition
    if lengthToSwap > len(listNumbers) :
        print("INVALID")
    else :
        listOfNumbersSwapping = getListOfNumbersToSwap(lengthToSwap)
        listOfNumbersSwapping.reverse()
        saveListOfNumbers(listOfNumbersSwapping)
        currentPosition = (currentPosition + lengthToSwap + skipSize) % len(listNumbers)
        skipSize += 1

map(goThroughNumbers,lengths)
print(listNumbers[0]*listNumbers[1])