from operator import add

def getWhichDirectionToTurn(currentState) :
    if currentState == "." :
        return "L"
    elif currentState == "W" :
        return "F"
    elif currentState == "#" :
        return "R"
    elif currentState == "F" :
        return "B"

def getNewDirection(currentDirection,directionToTurn) :
    directions=[[-1,0],[0,1],[1,0],[0,-1]]
    indexDir = directions.index(currentDirection)
    if directionToTurn == "R" :
        newDirection = directions[(indexDir+1)%4]
    elif directionToTurn == "L" :
        newDirection = directions[(indexDir-1)%4]
    elif directionToTurn == "F" :
        newDirection = currentDirection
    elif directionToTurn == "B" : 
        newDirection = directions[(indexDir+2)%4]
    return newDirection

def increaseGridUp(grid) :
    widthOfGrid = len(grid[0])
    newRow = ["." for x in range(0,widthOfGrid)]
    grid.insert(0,newRow)
    return grid

def increaseGridLeft(grid) :
    for x in range(0,len(grid)) :
        grid[x].insert(0,".")
    return grid

def increaseGridDown(grid) :
    widthOfGrid = len(grid[0])
    newRow = ["." for x in range(0,widthOfGrid)]
    grid.insert(len(grid),newRow)
    return grid

def increaseGridRight(grid) :
    widthOfGrid = len(grid[0])
    for x in range(0,len(grid)) :
        grid[x].insert(widthOfGrid,".")
    return grid

def getNextPositionAndState(currentPosition,direction,grid) :
    newPosition = map(add, currentPosition, direction)
    if newPosition[0] == -1 :
        grid = increaseGridUp(grid)
        newPosition[0] = 0

    elif newPosition[0] == len(grid) :
        grid = increaseGridDown(grid)

    elif newPosition[1] == -1 :
        grid = increaseGridLeft(grid)
        newPosition[1] = 0

    elif newPosition[1] == len(grid[0]) :
        grid = increaseGridRight(grid)
    
    newState = grid[newPosition[0]][newPosition[1]]
    return(newPosition,newState,grid)

def changeState(currentPosition,currentState,grid,currentCountOfInfections) :
    if currentState == "." :
        grid[currentPosition[0]][currentPosition[1]] = "W"
        newCountOfInfections = currentCountOfInfections
    elif currentState == "W" :
        grid[currentPosition[0]][currentPosition[1]] = "#"
        newCountOfInfections = currentCountOfInfections + 1
    elif currentState == "#" :
        grid[currentPosition[0]][currentPosition[1]] = "F"
        newCountOfInfections = currentCountOfInfections
    else :
        grid[currentPosition[0]][currentPosition[1]] = "."
        newCountOfInfections = currentCountOfInfections
    return (grid,newCountOfInfections)

def burst(currentPosition,currentDirection,grid,currentState,currentCountOfInfections) :
    directionToTurn = getWhichDirectionToTurn(currentState)
    newDirection = getNewDirection(currentDirection,directionToTurn)
    grid,newCountOfInfections = changeState(currentPosition,currentState,grid,currentCountOfInfections)
    (newPosition,newState,grid) = getNextPositionAndState(currentPosition,newDirection,grid)
    return(newPosition,newDirection,grid,newState,newCountOfInfections)

grid = """. . . . . . . . .
. . . . . . . . .
. . . . . . . . .
. . . . . # . . .
. . . # . . . . .
. . . . . . . . .
. . . . . . . . .
. . . . . . . . ."""

grid = """...#.##.#.#.#.#..##.###.#
......##.....#####..#.#.#
#..####.######.#.#.##...#
...##..####........#.#.#.
.#.#####..#.....#######..
.#...#.#.##.#.#.....#....
.#.#.#.#.#####.#.#..#...#
###..##.###.#.....#...#.#
#####..#.....###.....####
#.##............###.#.###
#...###.....#.#.##.#..#.#
.#.###.##..#####.....####
.#...#..#..###.##..#....#
##.##...###....##.###.##.
#.##.###.#.#........#.#..
##......#..###.#######.##
.#####.##..#..#....##.##.
###..#...#..#.##..#.....#
##..#.###.###.#...##...#.
##..##..##.###..#.##..#..
...#.#.###..#....##.##.#.
##.##..####..##.##.##.##.
#...####.######.#...##...
.###..##.##..##.####....#
#.##....#.#.#..#.###..##."""

grid = grid.split("\n")
#grid = [x.split(" ") for x in grid]
grid = [list(x) for x in grid]

currentPosition = [12,12]
#currentPosition = [4,4]
currentDirection = [-1,0]
#currentState="."
currentState="#"
currentCountOfInfections=0

i=0
while i < 10000000 :
    (currentPosition,currentDirection,grid,currentState,currentCountOfInfections)=burst(currentPosition,currentDirection,grid,currentState,currentCountOfInfections)
    #print(currentPosition,currentDirection,currentState)
    if i > 1000000045 :
        print("currentPosition")
        print(currentPosition)
        print("currentDirection")
        print(currentDirection)
        print("currentState")
        print(currentState)
    
    i+=1
print currentCountOfInfections