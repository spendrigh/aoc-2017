
inputFile = open("input.txt", "r+")

listOfLetters = inputFile.read()

#listOfLetters = "{{<a>},{<a>},{<a>},{<a>}}"

lastCharacterCancel = False
inRubbish = False
currentLevel = 0
score = 0
countRubbish = 0
def checkLetter(letter) :
    global score
    global lastCharacterCancel
    global inRubbish
    global currentLevel
    global countRubbish
    if lastCharacterCancel :
        lastCharacterCancel = False
        return 0
    if letter == "!" :
        lastCharacterCancel = True
        return 0
    if inRubbish :
        if letter == ">" :
            inRubbish = False
            return 0
        else :
            countRubbish += 1
            return 0
    if letter == "<" :
        inRubbish = True
        return 0
    if letter == "{" :
        #starting new object
        currentLevel += 1
        score += currentLevel
        return currentLevel
    if letter == "}" :
        currentLevel -= 1
    return 0

scores = [checkLetter(x) for x in listOfLetters]
print(scores)
print(score)
print(countRubbish)