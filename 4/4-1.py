inputFile = open("input.txt", "r+")

rawText = inputFile.read()

listOfPhrases = rawText.split("\n")

def CheckListOfWords(phrase) :
    list = phrase.split()
    # set(list) will only allow values if not duplicated
    # check the list of the list against the set
    noDuplicates = len(list) == len(set(list))
    return noDuplicates

goodList = filter(CheckListOfWords, listOfPhrases)

print("Number of good passphrases is:",len(goodList))