inputFile = open("input.txt", "r+")

rawText = inputFile.read()

listOfPhrases = rawText.split("\n")

def sortWord(word) :
    sortedWord = ''.join(sorted(word))
    return sortedWord

def CheckListOfWords(phrase) :
    list = phrase.split()
    sortedList = map(sortWord,list)
    # set(list) will only allow values if not duplicated
    # check the list of the list against the set
    noDuplicates = len(sortedList) == len(set(sortedList))
    return noDuplicates

goodList = filter(CheckListOfWords, listOfPhrases)

print("Number of good passphrases is:",len(goodList))