from operator import add

mapping = """     |          
     |  +--+    
     A  |  C    
 F---|----E|--+ 
     |  |  |  D 
     +B-+  +--+ 
""".split("\n")

inputFile = open("input.txt", "r+")

mapping = inputFile.read().split("\n")

x = 135
#x = where(|)
currentPosition=[0,x]
currentDirection=[1,0]
letters = ""
keepOnMoving = True

def getIcon(currentPosition) :
    if (currentPosition[0] < 0 or currentPosition[0] >= len(mapping) or currentPosition[1] < 0 or currentPosition[1] >= len(mapping[currentPosition[0]]) ) :
        return " "
    return mapping[currentPosition[0]][currentPosition[1]]

def getNextPosition(currentPosition,currentDirection, keepOnMoving,letters) :
    icon = getIcon(currentPosition)
    if (icon == "-" or icon == "|") :
        newPosition = map(add, currentPosition, currentDirection)
        return (newPosition,currentDirection,keepOnMoving,letters)
    elif icon == "+" :
        print("found +")
        if currentDirection[0] == 0 :
            print("currently moving horizontal")
            testDown = [1,0]
            testPosition = map(add, currentPosition, testDown)
            testIcon = getIcon(testPosition)
            print("testIcon1",testIcon)
            if (testIcon != "-" and testIcon != " ") :
                newPosition = testPosition
                newDirection = testDown
            else :
                testUp = [-1,0]
                testPosition = map(add, currentPosition, testUp)
                testIcon = getIcon(testPosition)
                print("testIcon2",testIcon)
                if (testIcon != "-" and testIcon != " ") :
                    newPosition = testPosition
                    newDirection = testUp
                
        else :
            print("currently moving vertical")
            testRight = [0,1]
            testPosition = map(add, currentPosition, testRight)
            testIcon = getIcon(testPosition)
            print("testIcon3",testIcon)
            if (testIcon != "|" and testIcon != " ") :#
                newPosition = testPosition
                newDirection = testRight
            else :
                testLeft = [0,-1]
                testPosition = map(add, currentPosition, testLeft)
                testIcon = getIcon(testPosition)
                print("testIcon4",testIcon)
                if (testIcon != "|" and testIcon != " ") :
                    newPosition = testPosition
                    newDirection = testLeft
        return (newPosition,newDirection,keepOnMoving,letters)
    elif icon == " " :
        print("STOPPING")
        keepOnMoving = False
        return(currentPosition,currentDirection,keepOnMoving,letters)
    else :
        print("Found letter", icon)
        newPosition = map(add, currentPosition, currentDirection)
        letters += icon
        return (newPosition,currentDirection,keepOnMoving,letters)
"""
def getNextPosition(currentPosition,currentDirection, keepOnMoving,letters) :
    print(True)
"""
steps = 0
print(currentPosition,currentDirection,keepOnMoving,letters)
while keepOnMoving :
    (newPosition,newDirection, keepOnMoving,letters) = getNextPosition(currentPosition,currentDirection,keepOnMoving,letters)
    steps += 1
    print newPosition
    print newDirection
    currentPosition = newPosition
    currentDirection = newDirection
print letters
print steps