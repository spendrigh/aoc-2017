inputFile = open("input.txt", "r+")

lengths = inputFile.read()

lengthsList = []
for i in range(0,128) :
    lengthsList.append("hfdlxzhv-"+str(i))
print lengthsList
listNumbers = range(0,256)
currentPosition = 0
skipSize = 0

def getListOfNumbersToSwap(lengthToSwap) :
    listOfNumbersSwapping = []
    if currentPosition + lengthToSwap >= len(listNumbers) :
        overFlow = currentPosition + lengthToSwap - len(listNumbers)
        listOfNumbersSwapping = listNumbers[ currentPosition : ] + listNumbers[ 0 : overFlow ]
    else :
        listOfNumbersSwapping = listNumbers[ currentPosition : (currentPosition+lengthToSwap) ]
    return listOfNumbersSwapping

def saveListOfNumbers(listOfNumbersSwapping) :
    global listNumbers
    if currentPosition + len(listOfNumbersSwapping) >= len(listNumbers) :
        overFlow = currentPosition + len(listOfNumbersSwapping) - len(listNumbers)
        firstList = listOfNumbersSwapping[ 0 : (len(listNumbers) - currentPosition) ]
        secondList = listOfNumbersSwapping[ (len(listNumbers) - currentPosition) : ]
        listNumbers = secondList + listNumbers[len(secondList) : -len(firstList) ] + firstList
    else :
        listNumbers = listNumbers[:currentPosition] + listOfNumbersSwapping + listNumbers[currentPosition+len(listOfNumbersSwapping):]

def goThroughNumbers(lengthToSwap) :
    global skipSize
    global currentPosition
    if lengthToSwap > len(listNumbers) :
        print("INVALID")
    else :
        listOfNumbersSwapping = getListOfNumbersToSwap(lengthToSwap)
        listOfNumbersSwapping.reverse()
        saveListOfNumbers(listOfNumbersSwapping)
        currentPosition = (currentPosition + lengthToSwap + skipSize) % len(listNumbers)
        skipSize += 1

# function that will split a list, l, into equal chunks of size n
def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]

def returnHash(length) :
    global lengths
    global listNumbers
    global currentPosition
    global skipSize
    lengths = length
    listNumbers = range(0,256)
    lengths = map(ord, lengths)

    lengthsOrig = lengths + [17, 31, 73, 47, 23]
    lengthsFull = []

    for i in range(0,64) :
        lengthsFull += lengthsOrig

    # being lazy and not changing old code to work with lengthsFull
    lengths = lengthsFull

    currentPosition = 0
    skipSize = 0


    map(goThroughNumbers,lengths)
    sparseHash = listNumbers

    chunksOfSparseHash = list(chunks(sparseHash,16))

    denseHash = []
    for individualChunk in chunksOfSparseHash :
        xor = reduce((lambda x, y: x ^ y), individualChunk)
        denseHash.append(xor)

    # turn ints into hex, with two digits padding
    hexValues = map('{:02x}'.format, denseHash)
    hexString = ''.join(hexValues)
    stringToHex = [int(x,16) for x in hexString]
    binaryValues = map('{:04b}'.format, stringToHex)
    binaryString = ''.join(binaryValues)
    binaryList = [int(x) for x in binaryString]
    return sum(binaryList)

output = map(returnHash,lengthsList)
print(sum(output))
#hexValues = map('{:04b}'.format, denseHash)
#print