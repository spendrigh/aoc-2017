inputFile = open("input.txt", "r+")

listOfInstructions = inputFile.read().split("\n")

register = {}

def snd(x) :
    # plays a sound with a frequency equal to the value of X.
    if checkIfInt(x) :
        x = int(x)
    else :
        if x not in register :
            register[x] = 0
        x = register[x]
    register["sound"] = x

def setReg(x,y) :
    # sets register X to the value of Y.
    if x not in register :
        register[x] = 0
    if checkIfInt(y) :
        y = int(y)
    else :
        if y not in register :
            register[y] = 0
        y = register[y]
    register[x] = y

def add(x,y) :
    # increases register X by the value of Y.
    if x not in register :
        register[x] = 0
    if checkIfInt(y) :
        y = int(y)
    else :
        if y not in register :
            register[y] = 0
        y = register[y]
    register[x] += y

def mul(x,y) :
    # sets register X to the result of multiplying the value contained in register X by the value of Y.
    if x not in register :
        register[x] = 0
    if checkIfInt(y) :
        y = int(y)
    else :
        if y not in register :
            register[y] = 0
        y = register[y]
    register[x] *= y

def mod(x,y) :
    # sets register X to the remainder of dividing the value contained in register X by the value of Y (that is, it sets X to the result of X modulo Y).
    if x not in register :
        register[x] = 0
    if checkIfInt(y) :
        y = int(y)
    else :
        if y not in register :
            register[y] = 0
        y = register[y]
    register[x] %= y

def rcv(x) :
    # recovers the frequency of the last sound played, but only when the value of X is not zero. (If it is zero, the command does nothing.)
    if checkIfInt(x) :
        x = int(x)
    else :
        if x not in register :
            register[x] = 0
        x = register[x]
    if x != 0 :
        return register["sound"]

def jgz(x,y,i) :
    # jumps with an offset of the value of Y, but only if the value of X is greater than zero. (An offset of 2 skips the next instruction, an offset of -1 jumps to the previous instruction, and so on.)
    if checkIfInt(x) :
        x = int(x)
    else :
        if x not in register :
            register[x] = 0
        x = register[x]
    if checkIfInt(y) :
        y = int(y)
    else :
        if y not in register :
            register[y] = 0
        y = register[y]
    if x > 0 :
        return i + y
    else :
        return i + 1


def checkIfInt(s):
    try: 
        int(s)
        return True
    except ValueError:
        return False

i=0
noRegisteredSound = True

while noRegisteredSound :
    print("----")
    print(i)
    print(listOfInstructions[i])
    print(register)
    splitBySpace = listOfInstructions[i].split()
    command = splitBySpace[0]
    x = splitBySpace[1]
    if len(splitBySpace) == 3 :
        y = splitBySpace[2]
        if checkIfInt(y) :
            y = int(y)
    if command == "snd" :
        snd(x)
    if command == "set" :
        setReg(x,y)
    if command == "add" :
        add(x,y)
    if command == "mul" :
        mul(x,y)
    if command == "mod" :
        mod(x,y)
    if command == "rcv" :
        sound = rcv(x)
        if sound :
            print("ending")
            print(sound)
            noRegisteredSound = False
            break
    if command == "jgz" :
        i = jgz(x,y,i)
        print(i)
    else :
        i += 1