inputFile = open("input.txt", "r+")

listOfInstructions = inputFile.read().split("\n")

savedList0=[]
savedList1=[]

def snd(x,program) :
    # plays a sound with a frequency equal to the value of X.
    global savedList0
    global savedList1
    if checkIfInt(x) :
        x = int(x)
    else :
        if x not in register[program] :
            register[program][x] = 0
        x = register[program][x]
    if program == 0 :
        savedList1.append(x)
    else :
        savedList0.append(x)

def setReg(x,y,program) :
    # sets register X to the value of Y.
    if x not in register[program] :
        register[program][x] = 0
    if checkIfInt(y) :
        y = int(y)
    else :
        if y not in register[program] :
            register[program][y] = 0
        y = register[program][y]
    register[program][x] = y

def add(x,y,program) :
    # increases register X by the value of Y.
    if x not in register[program] :
        register[program][x] = 0
    if checkIfInt(y) :
        y = int(y)
    else :
        if y not in register[program] :
            register[program][y] = 0
        y = register[program][y]
    register[program][x] += y

def mul(x,y,program) :
    # sets register X to the result of multiplying the value contained in register X by the value of Y.
    if x not in register[program] :
        register[program][x] = 0
    if checkIfInt(y) :
        y = int(y)
    else :
        if y not in register[program] :
            register[program][y] = 0
        y = register[program][y]
    register[program][x] *= y

def mod(x,y,program) :
    # sets register X to the remainder of dividing the value contained in register X by the value of Y (that is, it sets X to the result of X modulo Y).
    if x not in register[program] :
        register[program][x] = 0
    if checkIfInt(y) :
        y = int(y)
    else :
        if y not in register[program] :
            register[program][y] = 0
        y = register[program][y]
    register[program][x] %= y

def rcv(x,program,currentState) :
    global savedList0
    global savedList1
    if program == 0 :
        savedList = savedList0
    else :
        savedList = savedList1
    # recovers the frequency of the last sound played, but only when the value of X is not zero. (If it is zero, the command does nothing.)
    if len(savedList) == 0 :
        currentState[program] = 1
        return(currentState)
    else :
        nextValue = savedList[0]
        register[program][x] = nextValue
        if program == 0 :
            savedList0 = savedList0[1:]
        else :
            savedList1 = savedList1[1:]
        currentState[program]=0
        return(currentState)

def jgz(x,y,i,program) :
    # jumps with an offset of the value of Y, but only if the value of X is greater than zero. (An offset of 2 skips the next instruction, an offset of -1 jumps to the previous instruction, and so on.)
    if checkIfInt(x) :
        x = int(x)
    else :
        if x not in register[program] :
            register[program][x] = 0
        x = register[program][x]
    if checkIfInt(y) :
        y = int(y)
    else :
        if y not in register[program] :
            register[program][y] = 0
        y = register[program][y]
    if x > 0 :
        return i + y
    else :
        return i + 1

def checkIfInt(s):
    try: 
        int(s)
        return True
    except ValueError:
        return False

register = [{"p":0},{"p":1}]
count = 0

def singleIteration(program,i,currentState) :
    global count
    if i >= len(listOfInstructions) :
        print("END OF LINES")
        currentState[program] = 1
        return (i, currentState)
    print("--",program,"--")
    print(i)
    print(listOfInstructions[i])
    splitBySpace = listOfInstructions[i].split()
    command = splitBySpace[0]
    x = splitBySpace[1]
    if len(splitBySpace) == 3 :
        y = splitBySpace[2]
        if checkIfInt(y) :
            y = int(y)
    if command == "snd" :
        snd(x,program)
        if program == 1 :
            count += 1
    elif command == "set" :
        setReg(x,y,program)
    elif command == "add" :
        add(x,y,program)
    elif command == "mul" :
        mul(x,y,program)
    elif command == "mod" :
        mod(x,y,program)
    elif command == "rcv" :
        currentState = rcv(x,program,currentState)
        if currentState[0] == 1 and currentState[1] == 1 :
            print("STOPPING")
        elif currentState[program] == 1 :
            i -= 1
    if command == "jgz" :
        i = jgz(x,y,i,program)
    else :
        i += 1
    return (i, currentState)

continueToRun = True
i = 0
j = 0
currentState=[0,0]
while continueToRun :
    (i,currentState) = singleIteration(0,i,currentState)
    (j,currentState) = singleIteration(1,j,currentState)
    if currentState[0] == 1 and currentState[1] == 1 :
        continueToRun = False

print count
#7493