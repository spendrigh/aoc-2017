factorA = 16807
factorB = 48271

seedA = 289
seedB = 629

divisor = 2147483647

sumOfSame = 0

def returnGenA() :
    global seedA
    searchingForValue=True
    while searchingForValue :
        seedA = (seedA * factorA) % divisor
        if seedA % 4 == 0 :
            genABinary = "{0:032b}".format(seedA)
            genA16 = str(genABinary)[-16:]
            searchingForValue = False
            return genA16

def returnGenB() :
    global seedB
    searchingForValue=True
    while searchingForValue :
        seedB = (seedB * factorB) % divisor
        if seedB % 8 == 0 :
            genBBinary = "{0:032b}".format(seedB)
            genB16 = str(genBBinary)[-16:]
            searchingForValue = False
            return genB16


for i in range(0,5000000) :
    genA16 = returnGenA()
    genB16 = returnGenB()
    if genA16 == genB16 :
        sumOfSame += 1

print(sumOfSame)