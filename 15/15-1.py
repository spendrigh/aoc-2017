factorA = 16807
factorB = 48271

seedA = 289
seedB = 629

divisor = 2147483647

sumOfSame = 0

for i in range(0,40000000) :
    seedA = (seedA * factorA) % divisor
    genABinary = "{0:032b}".format(seedA)
    genA16 = str(genABinary)[-16:]

    seedB = (seedB * factorB) % divisor
    genBBinary = "{0:032b}".format(seedB)
    genB16 = str(genBBinary)[-16:]
    
    if genA16 == genB16 :
        sumOfSame += 1

print(sumOfSame)