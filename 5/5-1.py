inputFile = open("input.txt", "r+")

rawText = inputFile.read()

instructions = map(lambda x: int(x),rawText.split("\n"))
index = 0
lengthInstructions = len(instructions)

count = 0
while index < lengthInstructions :
    nextJump = instructions[index]
    instructions[index] += 1
    index += nextJump
    count += 1
print count