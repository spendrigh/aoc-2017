
inputFile = """p=<-717,-4557,2578>, v=<153,21,30>, a=<-8,8,-7>
p=<1639,651,-987>, v=<29,-19,129>, a=<-5,0,-6>
p=<-10482,-248,-491>, v=<4,10,81>, a=<21,0,-4>
p=<-6607,-2542,1338>, v=<-9,52,-106>, a=<14,2,4>"""

inputFile = open("input.txt", "r+").read()

particles = inputFile.split("\n")

def extractAcceleration(inputString) :
    print(inputString.split("a=<")[1])
    accelerationsString = inputString.split("a=<")[1].split(">")[0].split(",")
    accelerations = [int(x) for x in accelerationsString]
    totalAcceleration = (accelerations[0]**2 + accelerations[1]**2 + accelerations[2]**2)**0.5
    return totalAcceleration

accelerations = map(extractAcceleration,particles)
print(accelerations)
print(min(accelerations))
print(accelerations.index(min(accelerations)))
print(sorted(accelerations))
indices = [i for i, x in enumerate(accelerations) if x == min(accelerations)]
print(indices)