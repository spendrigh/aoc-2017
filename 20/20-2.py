from operator import add


inputFile = """p=<-6,0,0>, v=< 3,0,0>, a=< 0,0,0>    
p=<-4,0,0>, v=< 2,0,0>, a=< 0,0,0>
p=<-2,0,0>, v=< 1,0,0>, a=< 0,0,0>
p=< 3,0,0>, v=<-1,0,0>, a=< 0,0,0>"""

inputFile = open("input.txt", "r+").read()

particles = inputFile.split("\n")

def extractAcceleration(inputString) :
    accelerationsString = inputString.split("a=<")[1].split(">")[0].split(",")
    accelerations = [int(x) for x in accelerationsString]
    return accelerations

def extractVelocity(inputString) :
    velocitiesString = inputString.split("v=<")[1].split(">")[0].split(",")
    velocities = [int(x) for x in velocitiesString]
    return velocities

def extractPosition(inputString) :
    positionsString = inputString.split("p=<")[1].split(">")[0].split(",")
    positions = [int(x) for x in positionsString]
    return positions

accelerations = map(extractAcceleration,particles)
velocities = map(extractVelocity,particles)
positions = map(extractPosition,particles)

def findCollisions(positions) :
    # find duplicates
    dupes = [x for n, x in enumerate(positions) if x in positions[:n]]
    indices = []
    # get indicies of duplicate (seraches list for all dupes)
    for value in dupes :
        newindices = [i for i, x in enumerate(positions) if x == value]
        indices += newindices
    # remove duplicate indicies
    listOfIndicies = list(set(indices))
    #  sort
    listOfIndicies.sort()
    # reverse - too lazy to look up how to reverse sort above!
    listOfIndicies.reverse()
    return listOfIndicies

def removeCollisions(listToRemove,accelerations,velocities,positions) :
    if listToRemove :
        for x in listToRemove :
            del accelerations[x]
            del velocities[x]
            del positions[x]
    return (accelerations,velocities,positions)

for i in range(0,2000) :
    print(len(velocities))
    listToRemove = findCollisions(positions)
    (accelerations,velocities,positions) = removeCollisions(listToRemove,accelerations,velocities,positions)
    for j in range(0,len(velocities)) :
        velocities[j] = map(add,velocities[j],accelerations[j])
        positions[j] = map(add,velocities[j],positions[j])
    listToRemove = findCollisions(positions)
    (accelerations,velocities,positions) = removeCollisions(listToRemove,accelerations,velocities,positions)
    #answer=404