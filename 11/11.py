inputFile = open("input.txt", "r+")

directions = inputFile.read().split(",")
currentCoodinates = (0,0)
distances = []

def dist(p2):
    p1 = (0,0)
    y1, x1 = p1
    y2, x2 = p2
    du = x2 - x1
    dv = y2 - y1

    if ((du >= 0 and dv >= 0) or (du < 0 and dv < 0)) :
        return abs(du + dv)
    else :
        return max(abs(du), abs(dv))

def updateCoordiantes(direction,newCoordinates) :
    x, y = newCoordinates
    if direction == "se" :
        x += 1
    elif direction == "ne" :
        y += 1
    elif direction == "n" :
        y += 1
        x -= 1
    elif direction == "nw" :
        x -= 1
    elif direction == "sw" :
        y -= 1
    elif direction == "s" :
        x += 1
        y -= 1
    else :
        print("Bad direction")
    distances.append(dist((x,y)))
    return (x, y)

for direction in directions :
    currentCoodinates = updateCoordiantes(direction,currentCoodinates)

print("Part1:",dist(currentCoodinates))
print("Part2:",max(distances))

"""
    Question, can I somehow make filter on this:
    def updateCoordiantes(direction,coordinate) :
        x, y = coordinate
        if direction == "se" :
            x += 1
        elif direction == "ne" :
            y += 1
        elif direction == "n" :
            y += 1
            x -= 1
        elif direction == "nw" :
            x -= 1
        elif direction == "sw" :
            y -= 1
        elif direction == "s" :
            x += 1
            y -= 1
        else :
            print("Bad direction")
        return (x, y)

    where I input a list, directions, and the output of each filter operation is a tuple, coordinate = (x,y).
"""