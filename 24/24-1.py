inputFile = open("input.txt", "r+")

listOfComponents = """0/2
2/2
2/3
3/4
3/5
0/1
10/1
9/10"""


listOfComponents = inputFile.read()

listOfComponents = listOfComponents.split("\n")

listOfComponents = [x.split("/") for x in listOfComponents]

orderedComponents = {}

for x in range(0,len(listOfComponents)) :
    listOfComponents[x][0] = int(listOfComponents[x][0])
    listOfComponents[x][1] = int(listOfComponents[x][1])
    if listOfComponents[x][0] not in orderedComponents :
        orderedComponents[listOfComponents[x][0]] = []
    if listOfComponents[x][1] not in orderedComponents :
        orderedComponents[listOfComponents[x][1]] = []

    orderedComponents[listOfComponents[x][0]].append(listOfComponents[x])
    orderedComponents[listOfComponents[x][1]].append(listOfComponents[x])


"""
listOfBridges = [ [0,1], [1,5], [2,5] ]
listOfComponents = [ [0,1], [1,5], [2,5] ]

orderedComponents = { 0: [[0,1],[0,2]], 1: [[0,1],[1,5]],    2: [[2,5]],    5: [[1,5],[2,5]] )
"""

maxStrength = 0
maxLength = 0

def returnNextNumber(currentNumber,eachItem) :
    if eachItem[0] == currentNumber :
        return eachItem[1]
    else :
        return eachItem[0]

def searchForComponents(currentList,currentNumber,currentStrength,level,remainingList) :
    global maxStrength
    global maxLength
    for eachItem in orderedComponents[currentNumber] :
        #if level > 2 :
        #    a=1
        if eachItem in remainingList :
            nextNumber = returnNextNumber(currentNumber,eachItem)
            newStrength = currentStrength+sum(eachItem)
            lengthOfBridge = len(currentList)
            if lengthOfBridge >= maxLength :
                if lengthOfBridge == maxLength :
                    if newStrength > maxStrength :
                        maxStrength = newStrength
                else :
                    maxLength = lengthOfBridge
                    maxStrength = newStrength
            localList = list(currentList)
            localList.append(eachItem)
            localRemainingList = list(remainingList)
            localRemainingList.remove(eachItem)
            #print(len(localRemainingList))
            searchForComponents(localList,nextNumber,newStrength,level+1,localRemainingList)

searchForComponents([],0,0,0,listOfComponents)
print maxStrength