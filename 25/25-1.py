def update(state,slot,value,values) :
    if state == 1 :
        if value == 0 :
            value = 1
            newSlot = move(slot,"r")
        else :
            value = 0
            newSlot = move(slot,"l")
        state = 2
        values = writeValue(value,slot,values)
        (newValue,values,newSlot) = getNewValue(newSlot,values)
        return (state,newSlot,newValue,values)
    elif state == 2 :
        if value == 0 :
            newSlot = move(slot,"r")
            state = 3
        else :
            newSlot = move(slot,"l")
            state = 2
        values = writeValue(value,slot,values)
        (newValue,values,newSlot) = getNewValue(newSlot,values)
        return (state,newSlot,newValue,values)
    elif state == 3 :
        if value == 0 :
            value = 1
            newSlot = move(slot,"r")
            state = 4
        else :
            value = 0
            newSlot = move(slot,"l")
            state = 1
        values = writeValue(value,slot,values)
        (newValue,values,newSlot) = getNewValue(newSlot,values)
        return (state,newSlot,newValue,values)
    elif state == 4 :
        if value == 0 :
            value = 1
            newSlot = move(slot,"l")
            state = 5
        else :
            newSlot = move(slot,"l")
            state = 6
        values = writeValue(value,slot,values)
        (newValue,values,newSlot) = getNewValue(newSlot,values)
        return (state,newSlot,newValue,values)
    elif state == 5 :
        if value == 0 :
            value = 1
            newSlot = move(slot,"l")
            state = 1
        else :
            value = 0
            newSlot = move(slot,"l")
            state = 4
        values = writeValue(value,slot,values)
        (newValue,values,newSlot) = getNewValue(newSlot,values)
        return (state,newSlot,newValue,values)
    elif state == 6 :
        if value == 0 :
            value = 1
            newSlot = move(slot,"r")
            state = 1
        else :
            newSlot = move(slot,"l")
            state = 5
        values = writeValue(value,slot,values)
        (newValue,values,newSlot) = getNewValue(newSlot,values)
        return (state,newSlot,newValue,values)

def move(slot,dir) :
    if dir== "r" :
        return slot + 1
    else :
        return slot - 1

def writeValue(value,slot,values) :
    values[slot] = value
    return values

def getNewValue(newSlot,values) :
    if newSlot < 0 :
        values.insert(0,0)
        return (0,values,0)
    elif newSlot + 1 > len(values) :
        values.append(0) 
        return (0,values,newSlot)
    else :
        newValue = values[newSlot]
        return (newValue,values,newSlot)

state = 1
newSlot = 0
newValue = 0
values=[0]
for i in range(0,12586542) :
    (state,newSlot,newValue,values) = update(state,newSlot,newValue,values)
    if i % 10000 == 0 :
        print i
print(sum(values))