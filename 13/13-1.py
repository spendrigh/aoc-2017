inputFile = open("input.txt", "r+").read()

#inputFile = """0: 3
#1: 2
#4: 4
#6: 4"""

fireWallString = inputFile.split("\n")
fireWallList=[0] * ( int(fireWallString[-1].split(": ")[0]) + 1 )

def saveFireWallString(string) :
    splitString = string.split(": ")
    fireWallList[int(splitString[0])] = int(splitString[1])

map(saveFireWallString, fireWallString)

count = 0
for timeStep in range(0,len(fireWallList)) :
    if fireWallList[timeStep] != 0 :
        remainder = timeStep % ( fireWallList[timeStep] * 2 -2)
        if remainder == 0 :
            count += (timeStep * fireWallList[timeStep])
print(count)