inputFile = open("input.txt", "r+")

rawText = inputFile.read()
#remove trailing new line
rawText= rawText[:-1]

sum1 = 0
sum2 = 0

lenText = len(rawText)
midPoint = lenText / 2

for i,item in enumerate(rawText) :
	# Second challenge
	if i > midPoint-1 :
		if item == rawText[i-midPoint] :
			sum2 += int(item)
	elif item == rawText[i+midPoint] :
		sum2 += int(item)

	# First challenge
	if i+1 == len(rawText) :
		if item == rawText[0] :
			sum1 += int(item)
		break
	elif item == rawText[i+1] :
		sum1 += int(item)

print(sum1)
print(sum2)