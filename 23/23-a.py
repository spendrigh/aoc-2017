inputFile = open("input.txt", "r+")

listOfInstructions = inputFile.read().split("\n")

savedList0=[]
savedList1=[]

def setReg(x,y) :
    # sets register X to the value of Y.
    if x not in register :
        register[x] = 0
    if checkIfInt(y) :
        y = int(y)
    else :
        if y not in register :
            register[y] = 0
        y = register[y]
    register[x] = y

def sub(x,y) :
    # decreases register X by the value of Y.
    if x not in register :
        register[x] = 0
    if checkIfInt(y) :
        y = int(y)
    else :
        if y not in register :
            register[y] = 0
        y = register[y]
    register[x] -= y

def mul(x,y) :
    # sets register X to the result of multiplying the value contained in register X by the value of Y.
    if x not in register :
        register[x] = 0
    if checkIfInt(y) :
        y = int(y)
    else :
        if y not in register :
            register[y] = 0
        y = register[y]
    register[x] *= y

def jnz(x,y,i) :
    # jumps with an offset of the value of Y, but only if the value of X is non-zero. (An offset of 2 skips the next instruction, an offset of -1 jumps to the previous instruction, and so on.)
    if checkIfInt(x) :
        x = int(x)
    else :
        if x not in register :
            register[x] = 0
        x = register[x]
    if checkIfInt(y) :
        y = int(y)
    else :
        if y not in register :
            register[y] = 0
        y = register[y]
    if x != 0 :
        return i + y
    else :
        return i + 1

def checkIfInt(s):
    try: 
        int(s)
        return True
    except ValueError:
        return False

register = {"a":1,"b":0,"c":0,"d":0,"e":0,"f":0,"g":0,"h":0}
count = 0

def singleIteration(i,continueToRun) :
    global count
    if i >= len(listOfInstructions) :
        print("END OF LINES")
        continueToRun = False
        return (i,continueToRun)
    #print(i)
    #print(listOfInstructions[i])
    splitBySpace = listOfInstructions[i].split()
    command = splitBySpace[0]
    x = splitBySpace[1]
    if len(splitBySpace) == 3 :
        y = splitBySpace[2]
        if checkIfInt(y) :
            y = int(y)
    if command == "set" :
        setReg(x,y)
    elif command == "sub" :
        sub(x,y)
    elif command == "mul" :
        count+=1
        mul(x,y)
    if command == "jnz" :
        i = jnz(x,y,i)
    else :
        i += 1
    return (i, continueToRun)

continueToRun=True
i=0
while continueToRun :
    (i,continueToRun) = singleIteration(i,continueToRun)
    #if i % 50 == 0 :
    print register

print register["h"]