inputFile = open("input.txt", "r+").read()

rules = inputFile.split("\n")
#rules = ["../.# => ##./#../...",".#./..#/### => #..#/..../..../#..#"]

rulesSplit = [x.split(" => ") for x in rules]

rulesOrdered = {2: {}}

for x in rulesSplit :
    length = len(x[0])
    if length not in rulesOrdered :
        rulesOrdered[length] = {}
    rulesOrdered[length][x[0]] = x[1]

patterns = ".#./..#/###"
patterns = patterns.split("/")

def findRule(pattern) :
    patternLength = len(pattern)
    origPattern = pattern
    for i in range(0,9) :
        if pattern in rulesOrdered[patternLength] :
            return rulesOrdered[patternLength][pattern]
        pattern = changePattern(pattern,i,origPattern)
    print("CANNOT FIND RULE!!!")

def changePattern(pattern,i,origPattern) :
    if i == 3 :
        origPattern = origPattern.split("/")
        newPattern = []
        for i in origPattern :
            newPattern.insert(0,i)
    else :
        pattern = pattern.split("/")
        newPattern = zip(*pattern[::-1])
        newPattern = map(''.join,newPattern)
    pattern = '/'.join(newPattern)
    return pattern

def getSize(length) :
    size = -0.5 + 0.5 * (length*4 + 5)**0.5
    """
    2: 2^2 + 1 = 5
    3: 3^3 + 2 = 11
    4: 4*4 + 3
    """
    return size

def splitIntoXbyXBlocks(patterns,splitBy) :
    size = len(patterns[0])
    numberOfBlocksAcross = (size / splitBy)
    LargeBlocks=[]
    for x in range(0,numberOfBlocksAcross) :
        LargeBlocks.insert(x,[])
        for y in range(0,numberOfBlocksAcross) :
            LargeBlocks[x].insert(y,[])
            rowNumbers = range( (x*splitBy), (x*splitBy+splitBy) )
            columnNumbers = range( (y*splitBy), (y*splitBy+splitBy) )
            LargeBlocks[x][y] = returnSmallBlock(patterns,rowNumbers,columnNumbers)
    return LargeBlocks

def returnSmallBlock(patterns,rowNumbers,columnNumbers) :
    smallBlock=[]
    for x in range(0,len(rowNumbers)) :
        smallBlock.insert(x,[])
        for y in range(0,len(columnNumbers)) :
            smallBlock[x].insert(y,[])
            smallBlock[x][y] = patterns[rowNumbers[x]][columnNumbers[y]]
    smallBlock = map(''.join,smallBlock)
    smallBlock = '/'.join(smallBlock)
    return smallBlock

def updateWithNewBlocks(XbyXBlocks) :
    updatedXbyXBlocks = []
    for x in range(0,len(XbyXBlocks)) :
        updatedXbyXBlocks.insert(x,[])
        for y in range(0,len(XbyXBlocks[0])) :
            updatedXbyXBlocks[x].insert(y,[])
            rule = findRule(XbyXBlocks[x][y])
            updatedXbyXBlocks[x][y]=rule
    return updatedXbyXBlocks

def joinBackRows(singleRow) :
    splitValues = [x.split("/") for x in singleRow]
    joinBackValues = map(list,zip(*splitValues))
    finalOutput = '/'.join(map(''.join,joinBackValues))
    return finalOutput

def singleIteration(patterns) :
    lengthOfPatterns = len(patterns[0])
    if lengthOfPatterns % 2 == 0 :
        splitSize = 2
    else :
        splitSize = 3
    output = splitIntoXbyXBlocks(patterns,splitSize)
    output2 = updateWithNewBlocks(output)
    newPatterns = '/'.join(map(joinBackRows, output2))
    patterns = newPatterns.split("/")
    return patterns

for i in range(0,18) :
    print(i)
    patterns = singleIteration(patterns)

joinedPatterns = '/'.join(patterns)
print(joinedPatterns.count('#'))
# Answer1: 155
# Answer2: 2449665