inputFile = open("input.txt", "r+")
rawText = inputFile.read()
eachLine = rawText.split("\n")

def sortAndReturnDiff(inputList,partNumber) :
    # Split into list of numbers (int)
    listOfNumbers = map(lambda x: int(x),inputList.split("\t"))
    # Sort list
    sortedList = sorted(listOfNumbers)
    if partNumber == 1 :
        return sortedList[len(sortedList)-1] - sortedList[0]
    if partNumber == 2 :
        for i in reversed(sortedList) :
            for j in sortedList :
                if j <= i/2 :
                    if i % j == 0 :
                        return i/j

#First part
listOfDiffs = [sortAndReturnDiff(x, 1) for x in eachLine]
sumOfDiffs = sum(listOfDiffs)

#Second part
listOfDivisors = [sortAndReturnDiff(x, 2) for x in eachLine]
sumOfDivisors = sum(listOfDivisors)

print("Answer part 1:", sumOfDiffs)
print("Answer part 2", sumOfDivisors)