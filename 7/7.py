input = """pbga (66)
xhth (57)
ebii (61)
havc (66)
ktlj (57)
fwft (72) -> ktlj, cntj, xhth
qoyq (66)
padx (45) -> pbga, havc, qoyq
tknk (41) -> ugml, padx, fwft
jptl (61)
ugml (68) -> gyxo, ebii, jptl
gyxo (61)
cntj (57)"""

inputFile = open("input.txt", "r+")

input = inputFile.read()

programs = input.split("\n")

## The following allows to get/set a dictionary from a list of keys

from functools import reduce  # forward compatibility for Python 3
import operator

def getFromDict(dataDict, mapList):
    return reduce(operator.getitem, mapList, dataDict)

def setInDict(dataDict, mapList, value):
    getFromDict(dataDict, mapList[:-1])[mapList[-1]] = value

#objectOfParents = {"child":"parent","child":"parent"}
objectOfParents = {}
programTree = {}

def searchForParent(name, list) :
    if name in objectOfParents :
        parent = objectOfParents[name]
        list.append(parent)
        return searchForParent(parent,list)
    else:
        return list

for program in programs :
#    print("_____")
    programSplit = program.split(" -> ")
    name = programSplit[0].split(" ")[0]
    weight = int(programSplit[0].split(" ")[1].split('(')[1].split(')')[0])
    currentTree = {"weight": weight}
#    print("name:", name, "weight:",weight)
    if len(programSplit) == 2 :
        # have children
#        print("have children")
        children = programSplit[1].split(",")
        print(children)
        for child in children :
            child = child.split(" ")
            child = child[len(child)-1]
#            print("checking child", child)
            objectOfParents[child] = name
            if child in programTree :
#                print("child found")
                currentTree[child] = programTree[child]
            else:
                currentTree[child] = {}

    parentList = searchForParent(name,[])
    if len(parentList) == 0 :
#        print('noParents :(')
        programTree[name] = currentTree
    else :
        parentList.reverse()
        parentList.append(name)
        setInDict(programTree,parentList,currentTree)
parentList = searchForParent(name,[])
firstName = parentList[-1]

def getWeightOfPrograms(dic) :

    childWeights = []
    if len(dic) > 1 :
        for each in dic :
            if each != "weight" :
                weightOfChild = getWeightOfPrograms(dic[each])
                childWeights.append(weightOfChild)
        if min(childWeights) != max(childWeights) :
            print("WARNING! WEIGHTS WRONG: diff")
            print(childWeights)
            for each in dic : 
                print(dic[each]["weight"])
            print(max(childWeights) - min(childWeights))
    return dic["weight"] + sum(childWeights)

getWeightOfPrograms(programTree[firstName])
## For the second answer I didn't code the end. I knew the second child had to be 9 units lighter, so I just looked at the weight of the second child (1069) and subtracted 9