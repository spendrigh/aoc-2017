inputFile = open("input.txt", "r+").read()

commsList = inputFile.split("\n")

seenList=[0]
notSeenList = range(1,len(commsList))

def searchForProgram(program) :
    rawOutput = commsList[program]
    programList = map(lambda x: int(x), rawOutput.split("<-> ")[1].split(", "))
    for i in programList :
        if i not in seenList :
            seenList.append(i)
            notSeenList.remove(i)
            searchForProgram(i)

searchForProgram(0)
print("Part1:",len(seenList))

count = 1
while len(notSeenList) > 0 :
    searchForProgram(notSeenList[0])
    count += 1

print("Part2:",count)