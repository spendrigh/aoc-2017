
inputFile = open("input.txt", "r+")

input = inputFile.read()

"""input = b inc 5 if a > 1
a inc 1 if b < 5
c dec -10 if a >= 1
c inc -20 if c == 10"""

instructions = input.split("\n")

inMemory = {}

maxValueEver = 0

for instruction in instructions :
    instructionSplit = instruction.split(" if ")
    #condition splitting
    condition = instructionSplit[1]
    condition = condition
    conditionSplit = condition.split(" ")
    comparitor = conditionSplit[1]
    toCheck = conditionSplit[0]
    value = conditionSplit[2]


    if toCheck not in inMemory :
        inMemory[toCheck] = 0
    evalCondition = str(inMemory[toCheck]) + " " + comparitor + " " + value
    if eval(evalCondition) :
        #instruction splitting
        task = instructionSplit[0].split(" ")
        if task[0] not in inMemory :
            inMemory[task[0]] = 0
        multiplier = 1
        if task[1] == "dec" :
            multiplier = -1
        inMemory[task[0]] += multiplier * int(task[2])
        if inMemory[task[0]] > maxValueEver :
            maxValueEver = inMemory[task[0]]

print(inMemory)
max = 0
maxValue =""
for value in inMemory :
    if inMemory[value] > max :
        max = inMemory[value]
        maxValue = value
print max
print maxValueEver