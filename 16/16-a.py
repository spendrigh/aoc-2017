def spin(x, listOfProgs) :
    remainder = x % len(listOfProgs)
    newList = listOfProgs[-remainder:] + listOfProgs[:-remainder]
    return newList

def exchange(a,b,listOfProgs) :
    valA = listOfProgs[a]
    valB = listOfProgs[b]
    listOfProgs[a] = valB
    listOfProgs[b] = valA
    return listOfProgs

def partner(a,b,listOfProgs) :
    indexA = listOfProgs.index(a)
    indexB = listOfProgs.index(b)
    listOfProgs[indexA] = b
    listOfProgs[indexB] = a
    return listOfProgs

listOfProgs = [chr(i) for i in range(ord('a'),ord('p')+1)]

inputFile = open("input.txt", "r+")

instructions = inputFile.read().split(",")

for instruction in instructions :
    if instruction[0] == "s" :
        a = int(instruction[1:])
        listOfProgs = spin(a,listOfProgs)
    elif instruction[0] == "x" :
        a = int(instruction[1:].split("/")[0])
        b = int(instruction[1:].split("/")[1])
        listOfProgs = exchange(a,b,listOfProgs)
    elif instruction[0] == "p" :
        a = instruction[1:].split("/")[0]
        b = instruction[1:].split("/")[1]
        listOfProgs = partner(a,b,listOfProgs)
    else :
        print("Bad instruction")

print listOfProgs
listOfProgsString = ''.join(listOfProgs)
print listOfProgsString