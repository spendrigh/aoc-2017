def spin(x, listOfProgs) :
    remainder = x % len(listOfProgs)
    newList = listOfProgs[-remainder:] + listOfProgs[:-remainder]
    return newList

def exchange(a,b,listOfProgs) :
    valA = listOfProgs[a]
    valB = listOfProgs[b]
    listOfProgs[a] = valB
    listOfProgs[b] = valA
    return listOfProgs

def partner(a,b,listOfProgs) :
    indexA = listOfProgs.index(a)
    indexB = listOfProgs.index(b)
    listOfProgs[indexA] = b
    listOfProgs[indexB] = a
    return listOfProgs

listOfProgs = [chr(i) for i in range(ord('a'),ord('p')+1)]
initalListOfProgs = list(listOfProgs)
print(initalListOfProgs)
inputFile = open("input.txt", "r+")

instructions = inputFile.read().split(",")

newInstructions=[]
for instruction in instructions :
    if instruction[0] == "s" :
        a = int(instruction[1:])
        instruction = (0,a)
    elif instruction[0] == "x" :
        a = int(instruction[1:].split("/")[0])
        b = int(instruction[1:].split("/")[1])
        instruction = (1,a,b)
    elif instruction[0] == "p" :
        a = instruction[1:].split("/")[0]
        b = instruction[1:].split("/")[1]
        instruction = (2,a,b)
    newInstructions.append(instruction)

i = 0
while i < 10 :
    for instruction in newInstructions :
        if instruction[0] == 0 :
            a = instruction[1]
            listOfProgs = spin(a,listOfProgs)
        elif instruction[0] == 1 :
            a = instruction[2]
            b = instruction[1]
            listOfProgs = exchange(a,b,listOfProgs)
        elif instruction[0] == 2 :
            a = instruction[1]
            b = instruction[2]
            listOfProgs = partner(a,b,listOfProgs)
        else :
            print("Bad instruction")
    if listOfProgs == initalListOfProgs :
        print("YESSSSSSSSSSSSSSSSSSSSSSS")
        print(i)
    if (i % 1000000 == 0) :
        print i
    i += 1
print(listOfProgs)
"""
secondListOfProgs = list(listOfProgs)

exchangeList = []
for i in range(ord('a'),ord('p')+1) :
    exchangeList.append(listOfProgs.index(chr(i)))

def updateList(oldList,exchangeList) :
    newList = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]
    for i in range(0,16) :
        newList[exchangeList[i]] = oldList[i]
    return newList

millionList = list(initalListOfProgs)
i = 0
while i < 1000000 :
    millionList = updateList(millionList,exchangeList)
    if  i % 1000000 == 0  :
        print i
        print(millionList)
    i += 1
print('millionList')
print(millionList)

exchangeList = []
for i in range(ord('a'),ord('p')+1) :
    exchangeList.append(millionList.index(chr(i)))

billionList = list(initalListOfProgs)
while i < 1000 :
    billionList = updateList(billionList,exchangeList)
    if  i % 1000 == 0  :
        print i
        print(billionList)
    i += 1
"""
listOfProgsString = ''.join(listOfProgs)
print listOfProgsString
