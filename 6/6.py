inputFile = open("input.txt", "r+")

rawText = inputFile.read()

banks = map(lambda x: int(x),rawText.split("\t"))
previousBanks = []

def checkIfSeenBefore(banks) :
    output = filter(lambda x: banks == x, previousBanks)
    if len(output) > 0 :
        return True
    else:
        return False

def returnIndexAndValueOfMaxBank(banks) :
    currentIndex = 0
    maxIndex = 0
    maxValue = 0

    for i in banks :
        if i > maxValue :
            maxValue = i
            maxIndex = currentIndex
        currentIndex += 1
    return (maxIndex,maxValue)

haveSeenListBefore = False
while not haveSeenListBefore :
    (index,value) = returnIndexAndValueOfMaxBank(banks)
    banks[index] = 0
    while value > 0 :
        index += 1
        if index == len(banks) :
            index = 0
        banks[index] += 1
        value -= 1
    haveSeenListBefore = checkIfSeenBefore(banks)
    previousBanks.append(list(banks))
print("answer 1:", len(previousBanks))
print("answer 2:", len(previousBanks) - previousBanks.index(banks) -1)