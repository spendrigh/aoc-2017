step = 348

memory = [0]

currentLocation = 0

def updateMemory(memory,currentLocation) :
    newPosition = (step+currentLocation) % len(memory) + 1
    memory.insert(newPosition, len(memory))
    return(memory,newPosition)

for i in range(0,2017) :
    (memory,currentLocation) = updateMemory(memory,currentLocation)

print(memory[currentLocation+1])