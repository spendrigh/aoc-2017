step = 348

memory = [0,0]


currentLocation = 0

def updateMemory(currentLocation,lengthOfMemory) :
    newPosition = (step+currentLocation) % lengthOfMemory + 1
    if newPosition == 1 :
        print("yes")
        memory[1] = lengthOfMemory
    return(newPosition)
i=0
while i < 50000000 :
    currentLocation = updateMemory(currentLocation,i+1)
    if i % 1000000 == 0 :
        print i
    i += 1

print(memory)