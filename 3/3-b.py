# Determine what square it is in

input = 265149

squareWidth = 1
currentMax = 1
lastMax = 1
level = 0
squares = [0,1]
currentValue = 1

def oneStep(currentSquare,squareSide) :
    if squareSide == 0 :
        return currentSquare - (8*level -7) - 6
    elif squareSide == 1:
        return currentSquare - (8*level -7) - 4
    elif squareSide == 2:
        return currentSquare - (8*level -7) - 2
    else :
        return currentSquare - (8*level -7)

def getSquaresBehind(currentSquare,squareSide,distanceFromEdge) :
    if ( 
        squareSide != 3 and distanceFromEdge == (squareWidth - 2)
        ) or (
        squareSide == 3 and distanceFromEdge == (squareWidth - 3)
        ) :
        return [currentSquare -2, currentSquare -1]
    else : 
        return [currentSquare - 1 ]

def getSquaresStepped(currentSquare,squareSide,distanceFromEdge) :
    oneStepSquare = oneStep(currentSquare,squareSide)
    if  (
        distanceFromEdge == 0
        ) or (
            squareSide == 3 and distanceFromEdge == (squareWidth - 2)
        ) :
        if squareSide == 0 :
            return [(oneStepSquare - 1), oneStepSquare]
        if squareSide == 3 and currentSquare != 2 and distanceFromEdge == (squareWidth - 2)  :
            return [oneStepSquare + 1]
        else :
            return [oneStepSquare - 1]
    elif ( distanceFromEdge == 1 and squareSide != 0 ) :
        return [(oneStepSquare - 1), oneStepSquare]
    elif distanceFromEdge == 1 :
        return [(oneStepSquare - 1), oneStepSquare, (oneStepSquare +1)]
    elif (
        squareSide != 3 and  distanceFromEdge == (squareWidth - 2)
        ) or (
        squareSide == 3 and distanceFromEdge == (squareWidth - 3)
        ) :
        return [oneStepSquare, (oneStepSquare +1)]
    else :
        return [(oneStepSquare - 1), oneStepSquare, (oneStepSquare+1)]

def getConnectedSquares(currentSquare,squareWidth,distanceAroundSquare) :
    squareSide = (distanceAroundSquare) / (squareWidth -1 )
    distanceFromEdge = (distanceAroundSquare) % (squareWidth -1 )
    squaresBehind = getSquaresBehind(currentSquare, squareSide, distanceFromEdge)
    squaresStepped = getSquaresStepped(currentSquare, squareSide, distanceFromEdge)
    #Merge lists, ignoring duplicates
    resultList= list(set(squaresBehind)|set(squaresStepped))
    return resultList

while currentValue <= input :
    #increment
    level += 1
    squareWidth = (square*2) + 1
    currentMax = currentMax + (4*squareWidth)-4
    for i in range(lastMax + 1, currentMax +1) :
        currentSquare = i
        distanceAroundSquare = currentMax - currentSquare
        connectedSquares = getConnectedSquares(currentSquare,squareWidth, distanceAroundSquare)
        sum = 0
        for j in connectedSquares :
            sum = sum + squares[j]
        squares.append(sum)
        currentValue = sum
        if currentValue > input :
            break
    lastMax = currentMax
print("I think currentValue is", currentValue)