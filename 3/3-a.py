# Determine what square it is in

input = 265149

squareWidth = 1
currentMax = 1
level = 0
while currentMax < input :
    #increment
    level += 1
    squareWidth = (level*2) + 1
    currentMax = currentMax + (4*squareWidth)-4

distanceAroundSquare = currentMax - input
distanceFromEdge = (distanceAroundSquare) % (squareWidth -1 )

midDistance = (squareWidth - 1) / 2
dir1Length = midDistance
dir2Length = abs(distanceFromEdge - midDistance)

print("Answer:",dir1Length+dir2Length)